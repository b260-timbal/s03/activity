<?php 

class Person {

	// Properties
	public $firstName;
	public $middleName;
	public $lastName;

	//Constructor - a constructor is used during the creation of an object.
	public function __construct($firstName, $middleName, $lastName) {
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
	}

	//Methods
	public function printName() {
		return "Your full name is $this->firstName $this->middleName $this->lastName.";
	}

}

class Developer extends Person {
    public function printName() {
		return "Your name is $this->firstName $this->middleName $this->lastName and you are a developer.";
	}
}

class Engineer extends Person {
    public function printName() {
		return "Your are an engineer named  $this->firstName $this->middleName $this->lastName.";
	}
}

$person = new Person('Ronie John', 'Soriano', 'Timbal');

$developer = new Developer('John', 'Finch', 'Smith');

$engineer = new Engineer('Harold', 'Myers', 'Reese');
?>