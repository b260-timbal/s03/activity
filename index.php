<?php require_once "./code.php";?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S03: Activity</title>

    <style>
        body{
            background-color: black;
        }
        div {
            display: flex;
            border: 1px solid black;
            flex-direction: column;
            padding: 30px;
            justify-content: center;
            align-items: center;
            margin: auto;
            width: 50%;
            height: 50%;

            position: absolute;
            left: 0;
            right: 0;
            top: 100px;
            bottom: 100px;
            background-color: white !important;
        }

        h1{
            color: darkcyan;
        }

        p {
            font-weight: bold;
            font-style: italic;
        }
    </style>
</head>
<body>
   <div>
        <h1>Person</h1>
        <p><?php echo $person->printName();?></p>

        <h1>Developer</h1>
        <p><?php echo $developer->printName();?></p>

        <h1>Engineer</h1>
        <p><?php echo $engineer->printName();?></p>
   </div>
</body>
</html>